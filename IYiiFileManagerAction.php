<?php
/*
 	an extended class from YiiFileManagerRemoteAbstractAction must implements 
	this interface in order to be used as a valid Action instance.
 */
interface IYiiFileManagerAction {
	// must override:
	public function getDb();
	public function getFilemanager();
	// you can use default implementation for this methods:
	public function getApi();
	public function setCurrentUid($uid);
	public function storeData($identity, $uploaded_file, $fileman);
	public function isValidRequest($listener, $ipsource,&$error);
	public function onDataArrival($instance, $identity, $file_id);
	// must be used to give response to the caller end point
	public function onSuccess($transid="",$data=null);
	public function onError($error, $extra="",$transid="");
}
