<?php
/***
	Copy this class into your 'protected/commands'
 	
	run:

		cd protected
		# create and get some test listeners UID numbers:
		./yiic yiifilemanagerremotetester demolisteners	

		# send a message to a listener using a UID:
		./yiic yiifilemanagerremotetester demomessage --uid=XXX
 */
function filemanager(){
	// this function simulates the component: Yii::app()->filemanager
	$fileman = new YiiDiskFileManager();
	$fileman->storage_path = "/var/tmp/testaction";
	return $fileman;
}
// this class represents a listener class waiting for uploaded data or messages
//	the data was supposed to be sent via POST to a given URL. the URL was
//  created by an explicit call to: YiiFileManagerRemoteApi::newListener for
//  a given class name and model ID (an "identity")
class SomeListener 
	implements IYiiFileManagerRemoteListener {
	
	public function yiiFileManagerRemote_onDataArrival($identity, $file_id) {
		$message = filemanager()->read_file($identity, $file_id);
		printf(__METHOD__.", identity: %s, file_id: %s, message:\n[%s]\n",
			$identity, $file_id, $message);	
	}
}
// this class represents the URL handler (an action), is supposed to be called
// by a remote client using a URL previously obtained with: 
//	YiiFileManagerRemoteApi::newListener
// 
class SomeAction 
	extends YiiFileManagerAbstractAction 
		implements IYiiFileManagerAction {
	public function getDb(){
		return Yii::app()->db;
	}
	public function getFilemanager(){
		return filemanager();
	}
	public function onSuccess($tid="",$data=null){
		printf("transactionId successfull: %s, data(json)=%s\n",
			$tid,json_encode($data));
	}
	public function onError($a, $b="",$tid=""){
		printf(__METHOD__.": transid=%s, %s, %s\n",$tid, $a,$b);
	}
}
//
class YiiFileManagerRemoteTesterCommand extends CConsoleCommand {
	public function actionDemoListeners($classname="SomeListener"){
		// this methods perform a test and create some listeners
		$api = new YiiFileManagerRemoteApi();
		$db = Yii::app()->db;
		$identity = rand(1,10);
		printf("[newListener][%s,%s], result:",$identity,$classname);
		$uid = $api->newListener($db,$identity,$classname);
		printf("%s\n",$uid);
		printf("[findListener][%s], result (json):",$uid);
		printf("%s\n",json_encode($api->findListener($db, $uid)));
		printf("[canUse][%s in %s,%s], result: ",$uid, $identity, $classname);
		printf("%s\n",$api->canUse($db, $uid, $identity, $classname) ? "true":"false");
		printf("[findListenerBy][%s,%s], result:",$identity,$classname);
		printf("(json)%s\n",json_encode($api->findListenerBy($db,$identity,$classname)));
		printf("[list (default)](json):\n");
		foreach($api->listeners($db) as $r)
			printf("%s\n",json_encode($r));
	}
	public function actionDemoMessage($uid, $content='test content'){
		$action = new SomeAction("",0);
		printf("testing action.\n");
		$action->runaction($uid, array('content'=>$content,'transid'=>'ABC'),
				$action->getDb(), $action->getApi());
		printf("\ntesting done\n");
	}
}
