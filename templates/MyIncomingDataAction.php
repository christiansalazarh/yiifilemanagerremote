<?php 
/**
 	You must copy this class into 'protected/components/MyAction.php' (rename as you like it)
	
	Must provided appropiated values for:

		FOR					  	YOU SHOULD RETURN
		-------------------------------------------
		getDb()				: 	Yii::app()->db
		getFilemanager()	:	Yii::app()->fileman
		
 */
class MyIncomingDataAction 
	extends YiiFileManagerAbstractAction 
		implements IYiiFileManagerAction {
	public function getDb(){
		return Yii::app()->db;
	}
	public function getFilemanager(){
		return Yii::app()->fileman; 
	}
	public function onSuccess($transid="",$data=null){
		
	}
	public function onError($error, $extra="",$transid=""){

		parent::onError($error,$extra); // to display error into a log
	}
}
