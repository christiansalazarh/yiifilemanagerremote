<?php
/***
	this class handles a incomming post sent by a remote request to a given URL
	and safety pass the incoming data it into the listener class name and
	instance associated to the given URL by reading the UID argument.

	secuence:

	1. someone creates a "listener URL" for a given model instance, this url 
	was created by the usage of YiiFileManagerRemoteApi. It is supposed to
	be this instance (not the entire class) the one who is listening for data.

	2. somebody send a $_POST['content'] having "any large data on it"
	to the "listener URL".

	3. the web application catch the action and pass it to this class because
	is declarated in the 'class' entry on the actions() array for a 
	given controller.

	4. this class evaluate if the request is comming from a valid IP address
	previously declarated by the listener creator (step1).

	5. this class evaluate if the "listener url" has a valid listener model,
	such model must implements an interface in order to be recognized by this
	class (see also getInstanceFromListener).

	6. this class move the received data into a temporary local file,
	next convert it to "a file into the domain of a given identity" by the help
	of "YiiFileManager" (see also: storeData), and retrieve the resulting 
	file_id into the class who is listening for data (see: onDataArrival)

	a typicall URL could be:
                                                                              	
		"http://mysite.com/having/url/manager/site/remotedata/uid/THE_UID"
		"http://mysite.com/index.php?r=/site/remotedata&uid=THE_UID"
		"http://mysite.com/remotedata/THE_UID"

	the "remotedata" is the action name, defined in some place:

		class SiteController extends CController {
			public function actions()
			{
				return array(
					'remotedata'=>array(
						'class'=>'YiiFileManagerRemoteAction',
					),
				);
			}
		}

 * @author Christian Salazar H. <christiansalazarh@gmail.com>
 * @license http://opensource.org/licenses/bsd-license.php
 */
abstract class YiiFileManagerAbstractAction extends CAction {
	public function run($uid){
		$this->runaction($uid, $_POST, $this->getDb(), $this->getApi());
	}
// a default implementation for this interface methods:
	public function setCurrentUid($uid){ 
		// ?
	}
	public function getApi(){
		return new YiiFileManagerRemoteApi();
	}
	public function onSuccess($transactionid="",$data=null){
		// do nothing in this default implementation
		// maybe: echo "OK"; to 
	}
	public function onError($error,$extra="",$transactionid){
		Yii::log(__METHOD__."[".$error."][".$extra
			."][".$transactionid."]","error");
	}
	public function storeData($identity, $uploaded_file, $fileman) {
		return $fileman->add_files($identity, $uploaded_file);
	}
	public function isValidRequest($listener, $ipaddress,&$error){
		// must check if the current ipaddress is in the IP domain for this listener
		extract($listener);
		if(($ip_list=="") || ($ip_list=="*"))
			return true;
		foreach(explode(",",$ip_list) as $ip){
			// TODO: enchance the way in which this ip are compared
			if($ip == $ipaddress) // basic comparison way...sad, must enhance.
				return true;
		}
		$error="ip given ipaddress doesn't match any listener's ip_list rules.";
		return false;
	}
	public function onDataArrival($instance, $identity, $file_id){
		// default implementation. if you're using a CActiveRecord as host
		// then here you can:  	$model=$instance->model()->findByPk($identity);
		//	
		$instance->yiiFileManagerRemote_onDataArrival($identity, $file_id);
	}
// not part of the interface:
	protected function getInstanceFromListener($listener,$tid){
		extract($listener); // uid, identity_id, identity_class, enabed, ip_list
		if (!class_exists($identity_class, false)){
			$this->onError("class_not_found",json_encode($listener),$tid);
			return null;
		}else{
			// the identity_class must point to a class with no-argument constructor
			// 
			$instance = new $identity_class;
			$interface_name = "IYiiFileManagerRemoteListener";
			if($_implements = class_implements($instance)){
				if(isset($_implements[$interface_name])){
					return $instance;
				}else
				$this->onError("interface_required",$interface_name,$tid);
			}else
			$this->onError("interface_required",$interface_name,$tid);
			return null;
		}
	}
	protected function getIpAddress(){
		return Yii::app()->request->userHostAddress;
	}
	// returns a tmp local file containing the data received from the remote source
	protected function createLocalFile($data){
		$tmp = tempnam('/tmp','yiifilemanagerremote-');
		$f = fopen($tmp,"w");
		fwrite($f, $data);
		fflush($f);
		fclose($f);
		return $tmp;
	}
	protected function isDataAvailable($post,$boolQueryOnly=true){
		if($boolQueryOnly==true){
			return isset($post['content']);
		}
		else return $post['content'];
	}
	protected function getTransactionId($post){
		// a transactionid can be sent from the remote data source to identity
		// the data transaction, this same id will be returned to identity
		// a successfull transaction, see: onSuccess
		if(isset($post['transid']))
			return $post['transid'];
		return "";
	}
	public function runaction($uid, $post, $db, $api) {
		$tid=$this->getTransactionId($post);
		if(!$this->isDataAvailable($post)){
			$this->onError("no_data_available",
				"the 'content' post entry is not set",$tid);
			return false;// no data to be processed.
		}
		$this->setCurrentUid($uid);	//
		//
		// the first step is to find the URL by its UID
		$listener = $api->findListener($db, $uid);
		if($listener == null){
			$this->onError("invalid_uid",$uid, $tid);
			return false;
		}
		// nice..we have somebody in this application listening for incomming data,
		// now do instanciate this listener and hope in goods it is implementing 
		// the required client interface to receive the incoming data:
		$instance = $this->getInstanceFromListener($listener,$tid);
		if($instance == null)
			return false; // dont worry about describing error, it was done in method
		// OK all is fine, we have an instance waiting for data implementing the
		// required interface, but some security stuff must be performed:
		if($this->isValidRequest($listener, $this->getIpAddress(),$error)){
			//
			$tmpfile= $this->createLocalFile($this->isDataAvailable($post,false));
			if($tmpfile==null){
				$this->onError("no_local_file","can't create a local tmp file",$tid);
				return false;
			}
			$resp = $this->storeData($listener['identity_id'],$tmpfile, 
				$this->getFilemanager());
			@unlink($tmpfile);
			//
			if($resp == null){
				$this->onError("store_data","the local file cant be stored "
					."into the domain of a given identity",$tid);
				return false;
			}else{
				if(is_array($resp)){ foreach($resp as $item) 
					$file_id = $item; }else $file_id = $resp;
				$this->onDataArrival($instance, $listener['identity_id'], $file_id);
				// happy ending
				$this->onSuccess($tid, array($listener, $instance, $file_id));
				return true;
			}
		}else{
			$this->onError("invalid_request",$error,$tid);
			return false;
		}
	}
}
