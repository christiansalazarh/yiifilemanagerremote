SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `yiifilemanagerremote_listener`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `yiifilemanagerremote_listener` (
  `uid` VARCHAR(20) NOT NULL ,
  `identity_id` VARCHAR(45) NOT NULL ,
  `identity_class` VARCHAR(45) NOT NULL ,
  `enabled` INT NULL DEFAULT 1 ,
  `ip_list` BLOB NULL ,
  PRIMARY KEY (`uid`) ,
  INDEX `yiifilemanagerremote_complex` (`identity_id` ASC, `identity_class` ASC) )
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
