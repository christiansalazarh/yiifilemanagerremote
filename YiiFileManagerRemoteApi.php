<?php
/**
	YiiFileManagerRemoteApi 

	dependency:
		must create the table 'yiifilemanagerremote_listener' using the provided 
		sql script.

	Methods to create a UID associated to a given Class and Instance(an identity), 
	this UID can be included into a URL:

		http://myapp.com/index.php?r=/site/remotedata&uid=THE_UID

	When the action "remotedata" in the "site"Controller is invoked then the 
	CAction extended class can use the provided UID argument value to perform a 
	call to this API to findout the CLASSNAME and INSTANCE associated to this url.
	
	You create a UID using:  
		"UID = thisapi.newListener(IDENTITY, CLASSNAME)",
	And you retrieve the CLASSNAME and IDENTITY by calling:
		"data_array = thisapi.findListener(UID)"

	This api is used by YiiFileManagerAbstractAction in order to pass the incoming
	messages sent to the URL into a given CLASSNAME and INSTANCE (because IDENTITY is
	a unique ID in the classname domain)
 
 * @author Christian Salazar H. <christiansalazarh@gmail.com>
 * @license http://opensource.org/licenses/bsd-license.php
 */
class YiiFileManagerRemoteApi extends CApplicationComponent {
// public:
	/**
	  	insert a new record into the domain database
		
		@param object	$db	the database manager, example: Yii::app()->db or something else
	  	@param string $identity_id	the domain
		@param string $className	the domain
		@param boolean	$enabled	if this entry is enabled at startup
		@param boolean	$ip_list	"*"=all, "ip1;ip2;...;ipN", those ip passed to access control rules
	  	@returns string the new UID (or an existing one if trying to insert a duplicated domain)
	*/
	public function newListener($db, $identity_id, $className, $enabled=true, $ip_list="*"){
		if($r = $this->findListenerBy($db,$identity_id,$className))
			return $r['uid'];
		$uid = $this->UID($identity_id, $className);
		if($db->createCommand()->insert($this->getTablename(),
			array("identity_id"=>$identity_id, "identity_class"=>$className,
				"uid"=>$uid, "enabled"=>($enabled==true) ? 1 : 0, 
					"ip_list"=>$ip_list)))
			return $uid;
		return null;
	}
	/**
	 	finds a listener by its primarykey: the UID

		@param object	$db	the database manager, example: Yii::app()->db or something else
		@param string	$uid the UID used in a given URL (http://myapp/myurlrules/THE_UID/)
	 	@returns array indexed array, see also yiifilemanagerremote_url table def
	 */
	public function findListener($db, $UID){	
		return $db->createCommand()->select()->from($this->getTablename())
			->where("uid=:uid",
				array(":uid"=>$UID))->queryRow();
	}
	/**
	 	test for valid url usage into a specified domain

		@param object	$db	the database manager, example: Yii::app()->db or something else
	  	@param string $identity_id	the domain
		@param string $className	the domain
		@returns boolean true is this ID is in the provided domain
	*/
	public function canUse($db, $UID, $identity_id, $className){
		if($r = $this->findListener($db, $UID)){
			return (($r['identity_id'] == $identity_id) 
				&& ($r['identity_class'] == $className));
		}else return false;
	}
	/**
		find a record by domain

		@param object	$db	the database manager, example: Yii::app()->db or something else
	  	@param string $identity_id	the domain
		@param string $className	the domain
	 	@returns array indexed array, see also yiifilemanagerremote_url table def
	*/
	public function findListenerBy($db, $identity_id, $className){
		return $db->createCommand()->select()->from($this->getTablename())
			->where("identity_id=:id and identity_class=:cn",
					array(":id"=>$identity_id,":cn"=>$className))->queryRow();
	}
	/**
	

		@param object	$db	the database manager, example: Yii::app()->db or something else

		@param string $className the domain from which we want to read all entries
		@param string $order see also yiifilemanagerremote_url table def
	 	@returns array array of indexed array (items: see also yiifilemanagerremote_url table def)
	*/
	public function listeners($db,$className="",$order="identity_class"){
		$w = "(1)"; $p = array();
		if($className != ""){
			$w = "identity_class = :cn";
			$p = array(":cn"=>$className);
		}
		return $db->createCommand()->select()->from($this->getTablename())
			->where($w,$p)->order($order)->queryAll();
	}
// private:
	private function getTablename(){
		return "yiifilemanagerremote_listener";
	}
	private function UID($identity_id, $className){
		return hash("crc32",sprintf("%s-%s-%s-%s",
			$identity_id,$className,microtime(),rand(10000,99999))); 
	}
}
