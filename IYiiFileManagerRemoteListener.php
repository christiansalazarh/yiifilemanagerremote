<?php
/***
	Any class who is expecting to receive incoming data must implements 
	this interface.

	To register a class as a listener for a given message you are
	required to perform an api call to:

		$api = new YiiFileManagerRemoteApi();
		$URL = $api->newListener(Yii::app()->db, "MiClass", $model->primarykey);
 
	so, when somebody post a message to this URL then the class "MiClass"
	will receive the message by implementing the interface
  
 * @author Christian Salazar H. <christiansalazarh@gmail.com>
 * @license http://opensource.org/licenses/bsd-license.php
 */
interface IYiiFileManagerRemoteListener {
	/**
	  	receive an available file (file_id) to be readed by 
		YiiFileManager::read_file for a given $identity ID number.
		the $identity is a value attached to the previously created URL because
		somebody creates an URL attached to a given model Class & Identity.

	 	example implementation:

			$model = Employee::model()->findByPk($identity);
			if($model != null){
				$content = Yii::app()->fileman->read_file($file_id);
				echo "Hello employee!"
					."some guy in internet send a message to you:".$content;
			}

		@param string $identity	Must be the model ID.
		@param string $file_id The file identitificator created by YiiFileManager::add_files.
		@returns void nothing is expected to be returned here.
	*/
	public function yiiFileManagerRemote_onDataArrival($identity, $file_id);
}
